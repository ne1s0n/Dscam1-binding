<?php
include_once('pseudoa_table.php');
include_once('selfbinding_table.php');
include_once('../common/conf.php');
include_once('../common/utils.php');
require AUTOLOAD;

/** Creates arff file from the passed protein description. For each protein
 * a certain amount of exon-variant codes are expected, and will be looked in
 * the stored $pseudo_table, which contains the pseudoaminoacid values. We
 * then subtract the values for the first exon, first protein to those of
 * first exon, second protein, and the obtained absolute difference go into
 * the arff (together with the other two exons, after the same treatment).
 * Result will be specified in file $outfile, or returned as string
 * if $outfile is NULL (the default).
 * $proteins array of array, each element has six position, first three 
 *           describe first protein, second three second protein
 * $outfile string, filename, or NULL
 */ 
function make_arff($proteins, $outfile=NULL){
	global $arff_header;
	global $pseudo_table;
	$res = $arff_header;
	
	//adding the absolute difference in pseudoaminoacids sequence
	foreach ($proteins as $p){
		$res .=	implode(',', pseudoam_abs_diff($pseudo_table[$p[0]], $pseudo_table[$p[3]])).',';	//exon 4
		$res .=	implode(',', pseudoam_abs_diff($pseudo_table[$p[1]], $pseudo_table[$p[4]])).',';	//exon 6
		$res .=	implode(',', pseudoam_abs_diff($pseudo_table[$p[2]], $pseudo_table[$p[5]])).',';	//exon 9
		$res .=	"?\n";  //this is the binding class, unknown because it needs to be predicted
	}
	
	//should we return the content of the file, or save it?
	if (is_null($outfile)){
		return ($res);
	}else{
		file_put_contents($outfile, $res);
	}
}

/* This function:
 * 1) parses the fasta file
 * 2) parses the pairs file, 
 * 3) align exons to proteins cited in pairs file, filling the log file
 *    (errors and warnings, but also OK message)
 * 4) creates the exons structure - an array with an entry for each protein
 *    pair. Each entry is an associative array, with the folowing fields:
 * - 'prot1'  => first protein name
 * - 'prot2'  => second protein name
 * - 'desc'   => "($prot1, $prot2)",
 * - 'valid'  => TRUE if it's a valid, processable pair. FALSE otherwise
 * - 'warning'=> TRUE there are warnings for the user
 * - 'self'   => TRUE if it's a self binding pair (means prot1 and prot2 define the same protein)
 * - 'exons1' => array, three slots, exons of first protein (if valid is FALSE no content guaranteed)
 * - 'exons2' => array, three slots, exons of second protein (if valid is FALSE no content guaranteed)
 * - 'exons_all' => array, six slots, exons of first and second protein (if valid is FALSE no content guaranteed)
 * */
function exons_structure_construct_from_fasta($infolder, $config){
	//opening the log file
	file_put_contents(
		$infolder . '/Dscam.log', 
		"Dscam predictions from input exons file - Run $config[run_id]\n",
		FILE_APPEND);

	//read fasta file
	$fasta = parse_fasta($infolder . '/' . $config['inputproteins_fasta']);
	//read pairs file
	$pairs = parse_pair_file($infolder . '/' . $config['inputproteins_pairs'], array_keys($fasta['proteins']));
	//reduce the fasta file to only used proteins
	$proteins_used = array_intersect_key($fasta['proteins'], array_flip($pairs['unique']));
	
	//translate all proteins to exons coordinates
	$exons = array();
	$sequence_with_problems = array();
	foreach ($proteins_used as $protein => $sequence){
		$exons[$protein] = sequence_to_exons($sequence);

		//keeping track of errors, warnings and logs
		foreach ($exons[$protein]['errors'] as $e){
		file_put_contents(
			$infolder . '/Dscam.log', 
			'[ERROR][' . $protein . '] ' . $e . "\n",
			FILE_APPEND);
		}
		foreach ($exons[$protein]['warnings'] as $w){
		file_put_contents(
			$infolder . '/Dscam.log', 
			'[WARNING][' . $protein . '] ' . $w . "\n",
			FILE_APPEND);
		}
		foreach ($exons[$protein]['logs'] as $l){
		file_put_contents(
			$infolder . '/Dscam.log', 
			'[LOG][' . $protein . '] ' . $l . "\n",
			FILE_APPEND);
		}
	}

	//each pair must be examined
	$pair_desc = array();
	foreach ($pairs['pairs'] as $p){
		$prot1_name = $p[0];
		$prot2_name = $p[1];
		$prot1 = $exons[$prot1_name];
		$prot2 = $exons[$prot2_name];
		$pair_desc[] = array(
			'prot1'  => $prot1_name,
			'prot2'  => $prot2_name,
			'desc'   => "($prot1_name, $prot2_name)",
			'valid'  => (count($prot1['errors']) + count($prot2['errors'])) == 0,
			'warning' => (count($prot1['warnings']) + count($prot2['warnings'])) > 0,
			'self'   => $prot1['exons'] === $prot2['exons'],
			'exons1' => $prot1['exons'],
			'exons2' => $prot2['exons'],
			'exons_all' => array_merge($prot1['exons'], $prot2['exons'])
		);
	}

	return($pair_desc);
}

/* This function:
 * 1) parses the exons description file
 * 2) creates the exons structure - an array with an entry for each protein
 *    pair. Each entry is an associative array, with the folowing fields:
 * - 'prot1'  => first protein name
 * - 'prot2'  => second protein name
 * - 'desc'   => "($prot1, $prot2)",
 * - 'valid'  => TRUE if it's a valid, processable pair. FALSE otherwise
 * - 'warning'=> TRUE there are warnings for the user
 * - 'self'   => TRUE if it's a self binding pair (means prot1 and prot2 define the same protein)
 * - 'exons1' => array, three slots, exons of first protein (if valid is FALSE no content guaranteed)
 * - 'exons2' => array, three slots, exons of second protein (if valid is FALSE no content guaranteed)
 * - 'exons_all' => array, six slots, exons of first and second protein (if valid is FALSE no content guaranteed)
 * It also fills the log file (errors and warnings, but also OK message)
 * */
function exons_structure_construct_from_exon_file($infolder, $config){
	//opening the log file
	file_put_contents(
		$infolder . '/Dscam.log', 
		"Dscam predictions from input exons file - Run $config[run_id]\n",
		FILE_APPEND);

	$desc = parse_protein_description(($infolder . '/' . $config['inputproteins']));
	$res = array();
	
	for ($i = 0; $i < count($desc['proteins']); $i++){
		$res[] = array(
			'prot1'  => '',
			'prot2'  => '',
			'desc'   => $desc['notes'][$i],
			'valid'  => TRUE,
			'warning' => FALSE,
			'self'   => in_array($i, $desc['self_binding']),
			'exons1' => array_slice($desc['proteins'][$i], 0, 3),
			'exons2' => array_slice($desc['proteins'][$i], 3, 3),
			'exons_all' => $desc['proteins'][$i]
		);
	}
	
	return ($res);
}

/* Given a sequence, this function tries to align the three exons sequences
 * on it (using blastp, BLASTP_EVALUE read from config file).
 * If some exon variation aligns, that is taken as present.
 * If nothing aligns, the exon is not present and an error is creater.
 * If the best aligning sequence is incomplete (partial alignment, 
 * shorter, and so) a warning is created.
 * 
 * Returns an array with the following keys:
 * - exons: array of coded exon variants, keys can be '4', '6' and '9', values are variant numbers
 * - errors: array of error messages, empty in case of no error present
 * - warnings: array of warning messages, empty in case of no warning present
 * - logs: array of log messages, important to be recorded but not critical
 */
function sequence_to_exons($sequence){
	$exons = array();
	$errors = array();
	$warnings = array();
	$logs = array();
		
	//create temp fasta file with sequence
	$seq_fasta_file = tempnam(DATA_FOLDER, 'tmpfasta');
	file_put_contents($seq_fasta_file, $sequence);
	
	//storing the exons sequences for easy access
	$exons_fasta = array(
		'4' => EXON_4_FASTA_SERVER,
		'6' => EXON_6_FASTA_SERVER,
		'9' => EXON_9_FASTA_SERVER
	);
	
	//for each exon
	foreach ($exons_fasta as $e => $f){
		//building and executing the blast command, plus sorting by
		//evalue and filtering of only the first line
		$evalue = constant('BLASTP_EXON_' . $e . '_EVALUE');
		$cmd = "../blast/blastp -max_target_seqs 1 -evalue $evalue -task blastp-short -outfmt \"6 qseqid sseqid evalue length nident pident\" -subject $f -query $seq_fasta_file | env LC_ALL=C sort -k+11 -g | head -n 1";
		$cmd_res = shell_exec($cmd);

		//if nothing is returned, nothing aligns
		if (is_null($cmd_res)){
			$errors[] = 'No variant of exon ' . $e . ' was found in protein sequence.';
			$exons[$e] = 'NA';
		}else{
			//the aligning variant is in the second column
			$cmd_res = split("\t", trim($cmd_res));
			$exons[$e] = 'ex'.$cmd_res[1];
			
			//was it an incomplete alignment? let's check length and
			//percentage of identical alignments
			$length = constant('BLASTP_EXON_' . $e . '_LENGTH');
			$pident = constant('BLASTP_EXON_' . $e . '_PIDENT');
			if (intval($cmd_res[3]) < $length || floatval($cmd_res[5]) < $pident){
				$warnings[] = 'Exon ' . $e . " resulted in partial alignment: 
 - Found alignment evalue: $cmd_res[2] (required: $evalue)
 - Found alignment length: $cmd_res[3] (required: $length)
 - Found percentage of identities: $cmd_res[5] (required: $pident)";
			}else{
				$logs[] = 'Exon ' . $e . " was correctly aligned: 
 - Found alignment evalue: $cmd_res[2] (required: $evalue)
 - Found alignment length: $cmd_res[3] (required: $length)
 - Found percentage of identities: $cmd_res[5] (required: $pident)";
			}
		}
	}
	
	//cleaning after myself
	unlink($seq_fasta_file);

	//and we are done
	return(array(
		'exons' => $exons,
		'errors' => $errors,
		'warnings' => $warnings,
		'logs' => $logs
	));
}

/*
 * The two passed array are expected to be of the same size and contain
 * only numeric data. A cell-by-cell absolute difference is returned.
 * */
function pseudoam_abs_diff($pseudo1, $pseudo2){
	$res = array();
	for ($i=0; $i<count($pseudo1); $i++){
		$res[$i] = abs($pseudo1[$i] - $pseudo2[$i]);
	}
	return $res;
}

/* parses a pairs file, checks the simple format (only protein names
 * taken from passed protein list, only two per line, space separated, 
 * comments start with semicolon, blank lines are skipped)
 * and returns an array with the following elements:
 * - 'pairs' : array, each element is a 2 slots array of protein names (strings)
 * - 'unique' : array of strings, reporting the list of all involved proteins
 * - 'errors' is an array of string, containing the parsing errors. If empty, no
 *    parsing error was found. They can be prefixed with the optional $msg_prefix
 */
function parse_pair_file($infile, $proteins, $msg_prefix = ''){
	//room for result
	$errors = array();
	$pairs = array();
	$unique = array();
	
	//opening passed file
	$fin = fopen($infile, 'r');
	
	if ($fin){
		//parsing line by line, with a counter
		$line_cnt = 0;
		while (($line = fgets($fin)) !== false) {
			$line_cnt++;
			
			//cleaning the clutter
			$line = trim($line);
			
			//empty lines are skipped
			if (strlen($line) == 0) continue;
			
			//lines starting with a semicolon are comments
			if ($line[0] == ';') continue;
			
			//trying to split the line in two
			$pieces = explode(' ', $line);
			if (count($pieces) != 2){
				$errors[] = $msg_prefix."Problem on pair file, line $line_cnt: only two proteins per line allowed.";
				continue;
			}
			
			//cleaning the pieces
			$pieces = array_map('trim',$pieces);
			
			//are the two pieces valid protein names?
			if (in_array($pieces[0], $proteins) && in_array($pieces[1], $proteins)){
				//good codes, let's store them
				$pairs[] = array($pieces[0], $pieces[1]);
				$unique[$pieces[0]] = '';
				$unique[$pieces[1]] = '';
				continue;
			}else{
				//bad codes, let's create an error
				$errors[] = $msg_prefix."Problem on pair file, line $line_cnt: selected proteins ($pieces[0], $pieces[1]) not present in fasta file.";
				continue;
			}
		}
	}else{
		$errors[] = $msg_prefix.'Problem opening file ' . $infile;
	}
	
	//and we are done
	return array(
		'errors' => $errors, 
		'pairs' => $pairs,
		'unique' => array_keys($unique));
}

/* parses a fasta file and returns an array with the following elements:
 * - 'proteins' : array, keys are proteins names (first word after '>' sign)
 * - 'errors' is an array of string, containing the parsing errors. If empty, no
 *    parsing error was found. Each error message can be prefixed with the
 *    optional passed string prefix
 */
function parse_fasta($infile, $msg_prefix = ''){
	//room for result
	$errors = array();
	$proteins = array();
	
	//opening passed file
	$fin = fopen($infile, 'r');
	
	//preparing temp variables
	$prot_name = '';
	$prot_seq = array();
	
	if ($fin){
		//parsing line by line, with a counter
		$line_cnt = 0;
		while (($line = fgets($fin)) !== false) {
			$line_cnt++;
			
			//cleaning clutter
			$line = trim($line);
			
			//empty lines are skipped
			if (strlen($line) == 0) continue;
			
			//lines starting with a semicolon are comments
			if ($line[0] == ';') continue;
			
			//lines starting with '>' trigger a new protein
			if ($line[0] == '>'){
				//saving the proteins so far, if we have one
				if ($prot_name != '') $proteins[$prot_name] = implode($prot_seq);
				
				//new protein name is the first word, excluding the > sign
				$pieces = explode(' ', $line, 2);
				$prot_name = trim(substr($pieces[0], 1));
				
				//a fresh sequence
				$prot_seq = array();
				
				//ready for next line
				continue;
			}
			
			//if we get here it's a sequence line: we sanityze to upper
			//case and to accepted chars only
			$line = strtoupper($line);
			$accepted_chars = 'ABCDEFGHIJKLMNOPQRSTUVWYZX*-';  //from wikipedia
			$valid_line = strlen($line) === strspn($line, $accepted_chars);
			
			if ($valid_line){
				//good sequence, let's store it
				$prot_seq[] = $line;
			}else{
				//we found a line with bad chars, let's invalidate the 
				//protein and create an error message
				$prot_name = '';
				$errors[] = $msg_prefix.'Found bad amino acid codes in line ' . $line_cnt;
			}
		}
		
		//the last protein must still be saved
		if ($prot_name != '') $proteins[$prot_name] = implode($prot_seq);

		//closing
		fclose($fin);
	}else{
		$errors[] = $msg_prefix.'Problem opening file ' . $infile;
	}
	
	//and we are done
	return array(
		'errors' => $errors, 
		'proteins' => $proteins);
}

/* parses the protein description file, coded as a sequence of six comma 
 * separated numbers, positional, and representing the three exons and 
 * the selected variants. An example line could be
 * 
 * 2,36,12,11,23,14
 * 
 * The first three numbers represent the first protein, the second three
 * the second protein. The first and fourth number represent exon 4 of the
 * first and second protein, respectively. In this case, variants 2 and 11
 * were selected.
 * Returns an array with the following elements:
 * - 'proteins' : array of array, each element a six cell array containing 
 *                the variant coded as 'exA.B' where A is the exon 
 *                number (4, 6, 9) and B is the selected variant (see 
 *                pseudoa_table.php for allowed values)
 * - 'notes' : array of strings, as many entries as proteins, containing 
 *             the optional "notes" field from exon description file (entries
 *             can be empty)
 * - 'errors' : array of string, containing the parsing errors. If 
 *              empty, no parsing error was found. Each error can be prefixed
 *              with the optional $msg_prefix
 * - 'self_binding' : array of row indexes that ask describe self binding 
 *                    proteins (first and second proteins are identical)
 */ 
function parse_protein_description($infile, $msg_prefix = ''){
	global $pseudo_table;
	$errors = array();
	$proteins = array();
	$notes = array();
	$self_binding = array();

	$f = fopen($infile, 'r');
	if (!$f){
		$errors[] =  $msg_prefix.'Impossible to open input file ' + $infile;
	}else{
		$cnt = 0;
		while (($l = fgets($f, 1000)) !== false) {			
			//cleaning the clutter
			$l = trim($l);
			
			//empty lines are skipped
			if (strlen($l) == 0) continue;
			
			//lines starting with a semicolon are comments
			if ($l[0] == ';') continue;

			//let's parse the file
			$pieces = explode(',', $l, 7);
			$pieces = array_map('trim',$pieces);
			
			if (count($pieces) < 6){
				$errors[] = $msg_prefix.'Expected 6 or 7 elements per line, found '.count($pieces). ' in line '.$l;
				continue;
			}
			
			//all exon values should be int
			if (!ctype_digit($pieces[0]) || 
				!ctype_digit($pieces[1]) || 
				!ctype_digit($pieces[2]) || 
				!ctype_digit($pieces[3]) || 
				!ctype_digit($pieces[4]) || 
				!ctype_digit($pieces[5]))
			{
				$errors[] = $msg_prefix.'Some values are not numeric in line: '.$l;
				continue;
			}
			
			//if we get here, good numbers. But are those valid variants?
			if ($pieces[0] <= 0 ||
				$pieces[1] <= 0 ||
				$pieces[2] <= 0 ||
				$pieces[3] <= 0 ||
				$pieces[4] <= 0 ||
				$pieces[5] <= 0 )
			{
				$errors[] = $msg_prefix.'Some values are less than zero in line: '.$l;
				continue;
			}
			
			//checking if passed exons/variants are valid, i.e. have an
			//existing key in $pseudoa_table
			
			//exon 4
			if (!array_key_exists('ex4.'.$pieces[0], $pseudo_table)){
				$errors[] = build_bad_exon_error_message(4, $pieces[0], $l);
				continue;
			}
			if (!array_key_exists('ex4.'.$pieces[3], $pseudo_table)){
				$errors[] = build_bad_exon_error_message(4, $pieces[3], $l);
				continue;
			}
			//exon 6
			if (!array_key_exists('ex6.'.$pieces[1], $pseudo_table)){
				$errors[] = build_bad_exon_error_message(6, $pieces[1], $l);
				continue;
			}
			if (!array_key_exists('ex6.'.$pieces[4], $pseudo_table)){
				$errors[] = build_bad_exon_error_message(6, $pieces[4], $l);
				continue;
			}
			//exon 9
			if (!array_key_exists('ex9.'.$pieces[2], $pseudo_table)){
				$errors[] = build_bad_exon_error_message(9, $pieces[2], $l);
				continue;
			}
			if (!array_key_exists('ex9.'.$pieces[5], $pseudo_table)){
				$errors[] = build_bad_exon_error_message(9, $pieces[5], $l);
				continue;
			}
			
			//if we get here is a good description, and we put it in the stash
			$proteins[] = array(
				'ex4.'.$pieces[0],
				'ex6.'.$pieces[1],
				'ex9.'.$pieces[2],
				'ex4.'.$pieces[3],
				'ex6.'.$pieces[4],
				'ex9.'.$pieces[5]
			);
			
			//is it self binding?
			if ($pieces[0] == $pieces[3] && 
			    $pieces[1] == $pieces[4] &&
			    $pieces[2] == $pieces[5]){
			
				$self_binding[] = $cnt;
			}
			
			//do we have a note?
			if (count($pieces) == 7){
				$notes[] = $pieces[6];
			}else{
				$notes[] = '';
			}
			
			//update row counter
			$cnt++;
		}	
		fclose($f);
	}

	return array(
		'errors' => $errors, 
		'proteins' => $proteins,
		'notes' => $notes,
		'self_binding' => $self_binding);
}

/**
 * Function to build user friendly error message when an exon variant
 * is not valid.
 * $exon is the exon number (4,6 or 9)
 * $passed_value is the not valid variant coming from user
 * $line is the whole input line, useful for user interface
 * */
function build_bad_exon_error_message($exon, $passed_value, $line){
	global $pseudo_table;
	$msg = 'Bad exon/variant combination: exon '.$exon.' variant '.$passed_value.' on line '.$line.'<br>';
	$msg .= 'Allowed combinations: ' . implode(', ', array_keys($pseudo_table));
	return $msg;
}

/*
 * Function to send email using PHPMailer, which requires to load its
 * library to function, so you should do:
 * 
 *     require AUTOLOAD;
 * 
 * before using this function.
 * Parameters:
 * $to           recipient address
 * $from         sender address
 * $from_name    sender name
 * $subject      email subject
 * $body         email body
 * $attach       path to file to be attached (can be NULL to indicate 
 *               no attachment)
 * $attach_name  attachment filename as it will appear to recipient (it 
 *               is read only if $attach is not NULL)
 * $guser        gmail username to be used for email sending
 * $gpwd         gmail password for $guser
 * */
function smtpmailer($to, $from, $from_name, $subject, $body, $attach, $attach_name, $guser, $gpwd) { 
	$res = array();

	$mail = new PHPMailer();  // create a new object
	$mail->IsSMTP(); // enable SMTP
	$mail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
	$mail->SMTPAuth = true;  // authentication enabled
	$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 465; 
	$mail->Username = GUSER;  
	$mail->Password = GPWD;           
	$mail->SetFrom($from, $from_name);
	$mail->Subject = $subject;
	$mail->Body = $body;
	$mail->AddAddress($to);
	
	if (!is_null($attach)){
		$mail->addAttachment($attach, $attach_name);
	}

	if(!$mail->Send()) {
		$res['mail_sent'] = FALSE;
		$res['error_info'] = $mail->ErrorInfo;
	} else {
		$res['mail_sent'] = TRUE;
	}
	
	return($res);
}

/* extracts the requested field from the passed exons structure and
 * returns in a compact form. Default behaviour is to exclude those
 * lines with valid field FALSE, but the filter can be turned off
 * using the $only_valid switch. */
function exons_structure_get_field($exons_structure, $field, $only_valid = TRUE){
	$res = array();
	foreach ($exons_structure as $e){
		if (!$only_valid || $e['valid']){
			$res[] = $e[$field];
		}
	}
	return($res);
}

/* Updates entries referring to self binding proteins using the self
 * binding model. Any previous binding-related field is overwritten */
function exons_structure_update_selfbinding(&$exons_structure){
	foreach ($exons_structure as &$e){
		//skipping non-self-binding entries
		if (!$e['self']) continue;
		
		$strength = get_self_binding_strength($e['exons1']); // exons1 == exons2
		
		//updating the binding parts
		$e['model'] = 'self_binding';
		$e['binding'] = floatval($strength) >= 4 ? 'YES' : 'NO';
		$e['binding_strength'] = $strength;
	}
}

/* Returns an array of string arrays, one per $exons_structure after a
 * first one containing header. Data becomes thus ready for table-like
 * representation (on screen and file).
 * */
function exons_structure_to_table($exons_structure){
	$res = array();
	
	//putting the header
	$res[] = array(
		'first_prot_exons', 'second_prot_exons', 
		'model', 'predicted_binding_strength', 'binding',
		'notes'
	);
	
	foreach ($exons_structure as $e){
		//in notes we put the desc field, plus some instruction in case
		//of not valid entries
		$n = $e['desc'];
		if (!$e['valid'] || $e['warning']){
			$n .= ' See log for details.';
		}
		
		$res[] = array(
			implode(' ', $e['exons1']),
			implode(' ', $e['exons2']),
			$e['model'], $e['binding_strength'], $e['binding'], 
			$n
		);
	}

	return ($res);
}

/* This function parses the passed weka output and includes it in the 
 * passed exons_structure, under the assumption that only valid entries
 * were passed to weka.
 * Nothing is returned.
 */
function exons_structure_add_weka(&$exons_structure, $weka_output){
	$current_entry = -1;
	$in_result_area = FALSE;
	foreach($weka_output as $line){
		//empty lines are skipped
		if (trim($line) == '') continue;
		
		//are we in the table containing results?
		if ($in_result_area){
			$current_entry++;
			//not valid entries are skipped until we find a valid one
			while(!$exons_structure[$current_entry]['valid']){
				//let's check the next entry
				$current_entry++;
				
				//This should never happen. It would mean that we have
				//more entries in the weka file than in exons_structure.
				//However we put this safeguard to at least break the
				//infinite cycle, just in case...
				if ($current_entry > count($exons_structure)){
					break;
				}
			}
			//splitting the string, variable number of spaces as separator
			preg_match('/[ ]*([0-9]*)[ ]*([0-9:?]*)[ ]*([0-9:?]*)[ ]*([0-9:?]*)/', $line, $matches);
			if (DEBUG_PRINTS){
				var_dump('========================================');
				var_dump($line);
				var_dump('------------');
				var_dump($matches);
			}
			
			//$matches[3] == 1:0 => NOT BINDING
			//$matches[3] == 2:1 => BINDING
			$exons_structure[$current_entry]['binding_strength'] = $matches[3] === '1:0' ? '<4' : '≥4';
			$exons_structure[$current_entry]['model'] = 'hetero_binding';
			$exons_structure[$current_entry]['binding'] = $matches[3] === '1:0' ? 'NO' : 'YES';
		}else{
			//looking for the table header
			if ($line == '    inst#     actual  predicted error prediction'){
				$in_result_area = TRUE;
			}
		}
	}
	
	//a final pass to put all the binding-related fields of the invalid
	//entries to NA
	foreach ($exons_structure as &$e){
		if (!$e['valid']){
			$e['binding_strength'] = 'NA';
			$e['model'] = 'NA';
			$e['binding'] = 'NA';
		}
	}
}

/* The passed folder must have a name following the pattern "rtp_<code>", 
 * and must contain the following files:
 *     - "classify.arff" file, to be fed to weka
 *     - "self_binding.txt" file, with the list of self binding proteins,
 *       that is, a zero based index of line in the original file that
 *       describe self binding
 *     - "info.txt" file with: mail where to write, name of the original file
 *     - the original file, as sent by the user
 * 
 * This function runs the weka model on the passed file, parses weka results, 
 * overwrites the results for self binding, creates a compressed 
 * archive with the result and the original file and sends it to the 
 * user.
 * Returns an array with the following keys:
 * - errors: array of error messages
 * - status: array of log message
 * */
function run_model($infolder){
	//---------------prepare result object
	$model_res = array(
		'errors' => array(),
		'status' => array()
	);
	
	//---------------extract the code from folder name
	$foldername = basename($infolder);
	if (!startsWith($foldername, 'rtp_')){
		$model_res['errors'][] = "The passed folder $infolder does not start with the proper 'rtp_'";
		return($model_res);
	}
	
	//if we get here, it's a valid-name data folder. Les't take notes and
	//extract the run id
	$model_res['status'][] = "Processing $infolder";
	$run_id = substr($foldername, 4);
	

	//---------------parse the config sectionless ini file
	$config = parse_ini_file($infolder . '/' . $run_id . '.ini', FALSE);
	
	//---------------if bulk_run_from_sequence mode we do fasta -> exons_stucture
	if ($config['mode'] == 'bulk_run_from_sequence'){
		$exons_structure = exons_structure_construct_from_fasta($infolder, $config);
	}
	
	//---------------if bulk_run mode we do exons file -> exons_stucture
	if ($config['mode'] == 'bulk_run'){
		$exons_structure = exons_structure_construct_from_exon_file($infolder, $config);
	}
	
	//---------------exons_stucture -> arff
	$data_file = "$infolder/classify.arff";
	$arff = make_arff(exons_structure_get_field($exons_structure, 'exons_all'), $data_file);
	 
	//---------------run the weka model
	$model = '../model/hetero.dscam.svm_lin.c.6.p.50.l.36.w.0.5.model';
	$cmd = "java -cp ../weka/weka.jar weka.Run weka.classifiers.meta.CostSensitiveClassifier -l $model -T $data_file -p 0";
	exec($cmd, $output, $return_value);
	exons_structure_add_weka($exons_structure, $output);
		
	//---------------correct for self binding
	exons_structure_update_selfbinding($exons_structure);

	//---------------create result file
	$fp = fopen("$infolder/binding_results.csv", 'w');
	foreach (exons_structure_to_table($exons_structure) as $e){
		fputcsv($fp, $e);
	}
	fclose($fp);
	
	//---------------create archive with result file and log
	$cmd = "zip -j $infolder/results.zip $infolder/binding_results.csv $infolder/Dscam.log";
	exec($cmd);
	
	//---------------send email to user
	$mail_res = smtpmailer(
		$config['email'],                     //to
		GUSER,                                //from email
		'Dscam server',                       //from name
		"Dscam binding results - job $run_id",//subject
		DEMON_MSG_BODY,                       //body
		"$infolder/results.zip",              //path to file to be attached
		'binding_results.zip',                //filename as it will appear to user
		GUSER,                                //gmail user (from conf)
		GPWD                                  //gmail password (from conf)
	);
	if ($mail_res['mail_sent']){
		$model_res['status'][] = 'Mail successfully sent to ' . $config['email'];
	}else{
		$model_res['errors'][] = 'Error sending mail to ' . $config['email'];
		$model_res['errors'][] = $mail_res['error_info'];
	}

	//and we are done
	return($model_res);
}

/*Returns the binding strength of a self binding protein.
 * $exons_variants is a 3 slots array with exon variants (e.g. 'ex4.3', 'ex9.12'...)
 * Output is the binding strength.*/
function get_self_binding_strength($exons_variants){
	global $selfbinding_table;
	
	//building the key
	$k = implode('_', $exons_variants);
		
	//returning the strength
	return ($selfbinding_table[$k]);
}
?>
