<?php
include_once('../common/conf.php');
include_once('../common/utils.php');
include_once('model_lib.php');

//where to log activity?
$log = DATA_FOLDER.'/demon.log';

//some interface
log_entry($log, "Dscam demon wakes up, examining folder ".DATA_FOLDER);

while (TRUE){
	//check if we need to stop
	$res = glob(DATA_FOLDER.'/kill_demon');
	if (count($res) > 0){
		log_entry($log, 'Terminated.');
		exec("rm -rf $res[0]");
		exit();
	}
	
	//check if we have a new folder ready to be processed
	$res = glob(DATA_FOLDER.'/rtp_*');
	if (count($res) > 0){
		//yes! let's process it (the process consumes and deletes the folder)
		log_entry($log, "The demon wakes up and process folder $res[0]");
		$model_res = run_model($res[0]);
		
		//was everything ok?
		if(count($model_res['errors']) > 0){
			//problems happened. 
			log_entry($log, "Errors with folder $res[0]");

			//Let's put the folder in the bad list
			//so we don't keep processing it
			$info = pathinfo($res[0]);
			$new_foldername = 'bad_'.substr($info['basename'], 4);
			$cmd = "mv $res[0] $info[dirname]/$new_foldername";
			exec($cmd);
		}else{
			//no problems, let's delete the now processed folder
			log_entry($log, " - all is fine, removing folder $res[0]");
			exec("rm -rf $res[0]");
		}
	}else{
		//nothing to process, let's sleep for a while
		sleep(DEMON_SLEEP);
	}
}
