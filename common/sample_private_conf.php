<?php
//path to the dscam software, on local server address
define('DSCAM_SERVER_ROOT', '/home/nelson/research/Dscam1-binding');

//the maximum size, in bytes, of an accepted input files
//1048576 bytes = 1024 KB = 1 MB
define('MAX_FILE_SIZE_EXONS', 10240); //10 KB
define('MAX_FILE_SIZE_PAIRS', 10240); //10 KB
define('MAX_FILE_SIZE_SEQUENCE', 512000); //500 KB

//base http address of the server, used to construct links
define('SERVER_HTTP_ADDRESS', 'http://localhost/Dscam1-binding/');

//Sample exon input when no user input is present
define('SAMPLE_EXON_FILE', join(array(SERVER_HTTP_ADDRESS, 'data/Dscam_sample_exon.csv')));

//Sample exon input when no user input is present
define('SAMPLE_FASTA_FILE', join(array(SERVER_HTTP_ADDRESS, 'data/Dscam_sample_sequences.fasta')));

//Sample exon input when no user input is present
define('SAMPLE_PROTEIN_COUPLES_FILE', join(array(SERVER_HTTP_ADDRESS, 'data/Dscam_sample_pairs.txt')));

//fasta files for exon variants: web addresses
define('EXON_4_FASTA', join(array(SERVER_HTTP_ADDRESS, 'data/exon4.fasta')));
define('EXON_6_FASTA', join(array(SERVER_HTTP_ADDRESS, 'data/exon6.fasta')));
define('EXON_9_FASTA', join(array(SERVER_HTTP_ADDRESS, 'data/exon9.fasta')));

//fasta files for exon variants: local server addresses
define('EXON_4_FASTA_SERVER', join(array(DSCAM_SERVER_ROOT, '/data/exon4.fasta')));
define('EXON_6_FASTA_SERVER', join(array(DSCAM_SERVER_ROOT, '/data/exon6.fasta')));
define('EXON_9_FASTA_SERVER', join(array(DSCAM_SERVER_ROOT, '/data/exon9.fasta')));

//should debug prints be activated?
define('DEBUG_PRINTS', TRUE);

//Working folder for temporary files
//this need to be outside the wwwroot
define('DATA_FOLDER', '../data');

//autoload file, used for activate libraries
define ('AUTOLOAD', 'vendor/autoload.php');

//how many seconds must the demon sleep when there's nothing to do?
define ('DEMON_SLEEP', 5);

define ('DEMON_MSG_BODY', 'This is an automatic message, please do not reply.
The attached file contains the results of the required analysis');

//the created data folders should change ownership to the this user, so
//it can delete it
define ('DEMON_USER', 'nelson');

//gmail configuration
define('GUSER', 'xxx@gmail.com'); // GMail username
define('GPWD', 'xxx password xxx'); // GMail password

//reCAPTCHA
define('DO_RECAPTCHA', FALSE);
define('RECAPTCHA_SITEKEY', 'xxx site key xxx');
define('RECAPTCHA_SECRETKEY', 'xxx private key xxx');

//------- blastp parameters ---------
//maximum evalue
define('BLASTP_EXON_4_EVALUE', 0.001);
define('BLASTP_EXON_6_EVALUE', 0.001);
define('BLASTP_EXON_9_EVALUE', 0.001);

//minimum length of alignment
define('BLASTP_EXON_4_LENGTH', 10);
define('BLASTP_EXON_6_LENGTH', 10);
define('BLASTP_EXON_9_LENGTH', 10);

//minimum percentage of perfect alignment
define('BLASTP_EXON_4_PIDENT', 40);
define('BLASTP_EXON_6_PIDENT', 40);
define('BLASTP_EXON_9_PIDENT', 40);









