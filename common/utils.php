<?php 

/** Enters an array, exits a string (html ul representation).
 * Used mainly for debug. */
function array2ul($ar){
  if (!is_array($ar)) return "<ul><li>$ar</li></ul>";
  $res = '<ul>';
  foreach($ar as $k => $v){
    $res .= "<li>[$k] ";
    if (is_array($v)){
      $res .= array2ul($v);
    }else{
      $res .= "$v";
    }
    $res .= "</li>";
  }
  $res .= '</ul>';
  return $res;
}

function print_r_tree($data, $return = FALSE){
    // capture the output of print_r
    $out = print_r($data, true);

    // replace something like '[element] => <newline> (' with <a href="javascript:toggleDisplay('...');">...</a><div id="..." style="display: none;">
    $out = preg_replace('/([ \t]*)(\[[^\]]+\][ \t]*\=\>[ \t]*[a-z0-9 \t_]+)\n[ \t]*\(/iUe',"'\\1<a href=\"javascript:toggleDisplay(\''.(\$id = substr(md5(rand().'\\0'), 0, 7)).'\');\">\\2</a><div id=\"'.\$id.'\" style=\"display: none;\">'", $out);

    // replace ')' on its own on a new line (surrounded by whitespace is ok) with '</div>
    $out = preg_replace('/^\s*\)\s*$/m', '</div>', $out);

    // add the javascript function toggleDisplay() and then the transformed output
    $out = '<script language="Javascript">function toggleDisplay(id) { document.getElementById(id).style.display = (document.getElementById(id).style.display == "block") ? "none" : "block"; }</script>'."\n$out";
    
    //add a pre tag, always nice...
    $out = tag_wrap($out, 'pre');
    
    if ($return){
      return ($out);
    }else{
      print($out);
    }
}

/**
 * Returns an html string with the $text wrapped in a $type tag,
 * with the specified $parameters (if present)
 */
function tag_wrap ($text, $tag, $parameters = array()){
  $res = "<$tag ";
  foreach ($parameters as $k => $v){
    $res .= "$k = \"$v\" ";
  }
  $res .= ">$text";
  $res .= "</$tag>";

  return $res;
}

/**
 * Reads and returns the passed field from the passed array,
 * after unsetting it from the array
 * */
function syphon(&$array, $field_name){
    $v = $array[$field_name];
    unset($array[$field_name]);
    return ($v);
  }
  
/* prints a <pre> tagged version of the print_r of the passed var. 
 * If return == TRUE the string is returned, otherwise (default)
 * is printed*/
function print_pre($var, $return = FALSE){
  $str = tag_wrap(print_r($var, TRUE), 'pre');
  if ($return){
    return ($str);
  }else{
    print($str);
  }
}

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}
function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}

/*This function receives an entry from $_FILES and checks that the file 
 * is ok (non empty, not too big, right type).
 * - $file an entry from $_FILES
 * - $msg_prefix a string to be anteponed to error messages
 * - $max_size must be an integer (maximum allowed size, in bytes)
 * - $type must be an array either a string or an array of strings of allowed file types, as reported in the "type" field
 * Returns an array, empty if no error is present, or full of error messages
 * if something went wrong.
 * For error codes meaning in $_FILES please refer to http://php.net/manual/en/features.file-upload.errors.php
 * */
function check_file($file, $max_size, $msg_prefix = '', $type = array()){
	$res = array();
	
	// Check file size: not empty
	if ($file['size'] == 0) {
		$res[] = $msg_prefix.'File is empty.';
	}

	// Check file size: max size
	if ($file['size'] > $max_size) {
		$res[] = $msg_prefix.'File too large. Max size: '.$max_size.'B found size: '.$file['size'].'B';
	}

	// Check file type
	if (!is_array($type)) $type = array($type);
	//if allowed file types are specified... 
	if (count($type) > 0){
		//we check the file type against the list
		if (!in_array($file['type'], $type)) {
			$res[] = $msg_prefix."File of type $file[type] not allowed. Allowed types: ".implode(', ', $type);
		}
	}
	
	//check if upload error
	if ($file['error'] != 0){
		$res[] = $msg_prefix."Error uploading file. Code reported: $file[error]";
	}

	return $res;
}

/*This fucntion receives a string with an email and checks if its valid.
 * Returns an array of error messages, which is empty if everything is fine
 * and the email can be used. */
function check_email($email){
	$res = array();
	
	//first thing needed is to remove clutter
	$email = ''.trim($email);
	
	//if empty, we are done
	if ($email == ''){
		$res[] = 'Email cannot be empty';
	}else{
		//removing bad characters
		$clean_email = filter_var($email,FILTER_SANITIZE_EMAIL);
		if ($email != $clean_email){
			$res[] = 'Email contains invalid characters: '.$email;
		}else{
			if (!filter_var($email,FILTER_VALIDATE_EMAIL)){
				$res[] = 'Email is not in valid format: '.$email;
			}
		}
	}
	return $res;
}

function log_entry($file, $msg){
	$script_name = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);
	// define current time and suppress E_WARNING if using the system TZ settings
	// (don't forget to set the INI setting date.timezone)
	$time = @date('[Y/M/d:H:i:s]');
	// write current time, script name and message to the log file
	file_put_contents($file, "$time ($script_name) $msg" . PHP_EOL, FILE_APPEND);
}

?>
