# Dscam webserver

The Dscam webserver receives a list of pairs of Dscam proteins and 
predicts if the two proteins would interact (bind) or not, and the interaction
magnitude (binding strength).

The project is written entirely in php and must run on linux environment.
The following calls to external tools are made:
* _blastp_ (./blast/blastp ...)
* _weka_ (java -jar ./weka/weka.jar ...)
* _zip_

The architecture is composed by the website (folder _webinterface_) and a demon
(folder _model_). The demon must be running on the webserver. A script to record
the demon in a Centos-like architectur is present.

# Developers

* (main developer) Nelson Nazzicari - nelson _DOT_ nazzicari _AT_ crea _DOT_ gov _DOT_ it
* (original idea) Simone Marini - simone _DOT_ marini _AT_ ufl _DOT_ edu

# Citing Dscam Webserver

Marini, Simone, Nelson Nazzicari, Filippo Biscarini, and Guang-Zhong Wang. "Dscam1 web server: online prediction of Dscam1 self-and hetero-affinity." Bioinformatics 33, no. 12 (2017): 1879-1880.

