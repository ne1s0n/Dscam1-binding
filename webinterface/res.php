<?php
include_once('../model/model_lib.php');
include_once('../common/utils.php');
include_once('lib.php');

put_page_header();
put_res_header();

$res = parse_input();

//some debug
if (DEBUG_PRINTS){
	$text = '<table border="1"><tr><td style="vertical-align:top">';
	$text .= "This is the POST variable";
	$text .= array2ul($_POST);
	$text .= '</td><td style="vertical-align:top">';
	$text .= "This is the FILES variable<br>";
	$text .= array2ul($_FILES);
	$text .= '</td><td style="vertical-align:top">';
	$text .= 'This is the $res[values] variable<br>';
	$text .= array2ul($res['values']);
	$text .= '</td></tr></table>';
	echo build_data_block('DEBUG PRINTS', $text, 'collapsible', 'ui-content', 'data-collapsed="false"');
}

if (count($res['errors']) != 0){
	//errors parsing the input from user, let's inform them
	$text = "<h2>The following error(s) were found:</h2>";
	$text .= array2ul($res['errors']);
	$text .=  '<a href="' .  SERVER_HTTP_ADDRESS . 'webinterface/index.php">Go back.</a>';
	echo build_data_block('Errors', $text, 'collapsible', 'ui-content', 'data-collapsed="false"');
}else{
	//input was ok, let's start the actual processing
	//make dir with "work in progress" name	
	$folder = uniqid();
	$wip_folder = DATA_FOLDER.'/wip_'.$folder;
	$rtp_folder = DATA_FOLDER.'/rtp_'.$folder;
	mkdir($wip_folder);
	
	//config and other important information end in a sectionless ini file
	$ini_filename = $wip_folder . '/' . $folder . '.ini';
	file_put_contents($ini_filename, 'email = ' . $res['values']['mail'] . "\n", FILE_APPEND);
	file_put_contents($ini_filename, 'mode = ' . $res['values']['mode'] . "\n", FILE_APPEND);
	file_put_contents($ini_filename, 'run_id = ' . $folder . "\n", FILE_APPEND);

	//move user input files to wip folder
	foreach($_FILES as $file_block_name => $file_block){
		$target_name = $file_block_name.'_'.$file_block['name'];
		move_uploaded_file($file_block['tmp_name'], $wip_folder.'/'.$target_name);
		file_put_contents($ini_filename, $file_block_name . ' = ' . $target_name . "\n", FILE_APPEND);
	} 
	
	//changing permission to folder, so later the demon can delete it	
	$cmd = "chmod -R a+w $wip_folder";
	exec($cmd, $output_cmd, $return_val);
	
	//rename folder to "ready to process" name
	rename($wip_folder,$rtp_folder);
	
	//message to user
	$text = "Job submitted with ID $folder. You'll receive the results at $_POST[email].<br>
		<a href=\"" . SERVER_HTTP_ADDRESS . 'webinterface/index.php">Go back.</a>';
	echo build_data_block('Job submitted', $text, 'collapsible', 'ui-content', 'data-collapsed="false"');
}

put_page_footer();

?>
