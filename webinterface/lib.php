<?php
include_once('../model/model_lib.php');
include_once('../common/conf.php');
include_once('../common/utils.php');

function put_page_header(){
        echo '
        <html><head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
        <script type="text/javascript">
			var CaptchaCallback = function() {
			grecaptcha.render(\'Recaptcha_bulk\', {\'sitekey\' : \'' . RECAPTCHA_SITEKEY . '\'});
			grecaptcha.render(\'Recaptcha_bulk_seq\', {\'sitekey\' : \'' . RECAPTCHA_SITEKEY . '\'});
			};
		</script>
        <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
        </head><body><div data-role="page" id="pageone">';
}

function put_page_footer(){
        echo "</div></body></html>";
}

function put_home_header(){
	    $text = '
		<div class="ui-bar ui-bar-b">
		<h3 class="header_title">
		<span id="header_container">Dscam1 webserver - binding predictions</span>
		<a href="http://www.bic.kyoto-u.ac.jp/"><img src="img/bic_logo.gif" class="left_logo"></a>
		<a href="http://crea.gov.it/"><img src="img/CREA-logo.png" class="left_logo"></a>
		<a href="http://www.ptp.it/"><img src="img/PTP_logo.png" class="right_logo"></a>
		<a href="http://www.picb.ac.cn/picb/peopleeng.jsp?ntype=pi&ID=30"><img src="img/CAS-MPG_logo.png" class="right_logo"></a>
		</h3>
		</div>
		';

        echo build_data_block('', $text, 'header', '');
}

function put_res_header(){
	    $text = '
		<div class="ui-bar ui-bar-b">
		<h3 class="header_title">
		<span id="header_container"><a id="header_link" href="'. SERVER_HTTP_ADDRESS. '">Dscam1 webserver - results</a></span>
		<a href="http://www.bic.kyoto-u.ac.jp/"><img src="img/bic_logo.gif" class="left_logo"></a>
		<a href="http://crea.gov.it/"><img src="img/CREA-logo.png" class="left_logo"></a>
		<a href="http://www.ptp.it/"><img src="img/PTP_logo.png" class="right_logo"></a>
		<a href="http://www.picb.ac.cn/picb/peopleeng.jsp?ntype=pi&ID=30"><img src="img/CAS-MPG_logo.png" class="right_logo"></a>
		</h3>
		</div>
		';

        echo build_data_block('', $text, 'header', '');
}

function put_res_content($msg){
        echo build_data_block('', $msg, 'main');        
}

function put_home_footer(){
        echo build_data_block('footer here, if needed', '', 'footer', '');
}

function put_main_block(){
        $text = '<p>
			Dscam1 webserver is a web tool implementing two prediction models for the affinity of self- and hetero-binding Drosphila 
			Dscam1 proteins. The models are based on support vector machine engines, learning from Chou\'s pseudo amino acid encoded features.<br>
			The results are sent to the user via email. No registration is required.<br>
			If you use this tool for your research please cite:</p>';
        $text .= '<p>
      S. Marini, N. Nazzicari, F. Biscarini and G-Z Wang
      <i>"Dscam1 Web Server: online prediction of Dscam1 self- and hetero-affinity"</i>
      Bioinformatics (2017) btx039 - In press.
			</p>';
        $text .= '<p>For inquiries contact dr. Simone Marini: 
			<a href="mailto:simone.marini@unipv.it?Subject=About%20Dscam1%20webserver" target="_top">simone.marini@unipv.it</a><br>
			For technical questions on server functionalities and installation contact Dr. Nelson Nazzicari:
			<a href="mailto:nelson.nazzicari@crea.gov.it?Subject=About%20Dscam1%20webserver" target="_top">nelson.nazzicari@crea.gov.it</a><br>
        </p>';
        $text .= '<p>The source code for this website, the underlying mathematical model and
        the queue system are freely available <a href="https://gitlab.com/ne1s0n/Dscam1-binding">here.</a></p>';
        
		echo build_data_block('Introduction', $text, 'collapsible', 'ui-content', 'data-collapsed="false"');
}

function put_motivation_block(){
	$text = '<p>
		Detailed biochemical studies of specific isoforms strongly suggest that homophilic binding, i.e. the formation of homodimers by identical Dscam1 isomers, is of great importance for the self-avoidance of growing neurites. Dscam1 proteins are playing a pivotal role in neural development [1].<br>
		Three Dscam1 extracellular Ig domains (Ig2, Ig3 and Ig7) are encoded by three variable exons (exon4, exon 6 and exon 9). Each variable domain is encoded by a corresponding cluster of multiple alternative exons, respectively 12 (exon 4), 48 (exon 6) and 33 (exon 9). Only one exon from each cluster is used through the stochastic expression process of alternative splicing, to express thousands of Dscam1 isoforms at a time [1].<br>
		This means there are <b>19008</b> possible domain combinations in case of self binding, and <b>350+ millions</b> (19008 x 19008) in case of hetero binding. Of these, we have actual measures of 89 self-binding samples and 1722 hetero-binding samples. We therefore developed two predicting models for the unmeasured binding combinations affinity.<br>
		</p>
		<h3>Self-binding</h3>
		<p>In case of self-binding, we have affinity measured in 89 samples, ranging from 1.06 to 58, according to a custom ELISA-based binding essay [2], with most of the combinations (95%+) having a binding strength >4. We therefore designed a regression model, achieving a Mean Absolute Error &le;10% on a test set not involved in any of the training phases, and useful to prove Dscam1 evolution in Drosophila [1].</p>
		<h3>Hetero-binding</h3>
		<p>In case of hetero-binding we have affinity measured in 1722 samples, ranging from 1 to 26.5, according to a custom ELISA-based binding essay [2], with most of the combinations (97%+) having a binding strength &le;4. For hetero-binding, therefore, it is important to predict if a sample belongs to the minority class, showing a moderate to high binding strength, comparable with the self-binding range.
		</p><p>
                We therefore designed a classification model, achieving a Precision of 0.958, a Recall of 0.819, and a F-measure of 0.877 on a test set not involved in any of the training phases.
		</p>
		<p align="center"><img src="img/vergogna.png" width="800"></p>
		<p>
		[1] Wang G-Z, Marini S, Ma X, Yang Q, Zhang X, Zhu Y. Improvement of Dscam1 homophilic binding affinity throughout Drosophila evolution. BMC Evolutionary Biology. 2014;14:186.<br>
		[2] Woj M., et al. A vast repertoire of Dscam1 binding specificities arises from modular interactions of variable Ig domains. Cell 130.6 (2007): 1134-1145.
		</p>	
	';

    echo build_data_block('Motivation', $text);
}

function put_model_design_block(){
	$text = '
		<h3>Self binding</h3>
        The self-binding model is based on Support Vector Regression. Here every variable exon sequence is 
        represented by Chou\'s Pseudo Amino Acids (PseAAs) [2]. The juxtaposed PseAAs, along with domain 
        information, are the features fed to the model. In this way, we obtain a fixed number of features
        from amino acid sequences with different lengths, embedding both frequency and positional information.
        For more details, check the Methods Section in Wang et al. [1].		
		</p>
		<h3>Hetero binding</h3>
		<p>
        The hetero-binding model is based on Support Vector Machines. As in the self-binding case, this model 
        exploits PseAAs to represent its features. However, for hetero-binding we have six variable exons 
        involved (three for each protein). Therefore the features provided as a input for the model are now 
        the absolute values of PseAAs differences. In other words, given two exon triplets A and B, we compute 
        PseAAs for A and B separately (as in the self-binding case); then we encode the feature by computing 
        the PseAA absolute difference.
        </p>
		<p>
		[1] Wang G-Z, Marini S, Ma X, Yang Q, Zhang X, Zhu Y. Improvement of Dscam1 homophilic binding affinity throughout Drosophila evolution. BMC Evolutionary Biology. 2014;14:186.<br>
		[2] Woj M., et al. A vast repertoire of Dscam1 binding specificities arises from modular interactions of variable Ig domains. Cell 130.6 (2007): 1134-1145.	
		</p>
	';
    echo build_data_block('Models design', $text);
}

function put_help(){
	$text = '
		If you already know which exon variants are present in your proteins you can directly input them 
		to the server. Otherwise, you should start from the protein sequences.
		<h3>From protein sequence</h3>
		<p>
		Use this box if you don\'t know which variants of the three Ig domains
		(<a href="'. EXON_4_FASTA . '" data-ajax="false">exon4</a>,
		<a href="'. EXON_6_FASTA . '" data-ajax="false">exon6</a>,
		and <a href="'. EXON_9_FASTA . '" data-ajax="false">exon9</a>)
		are present in your proteins.<br>
		You are required to input two files:
		<ul>
		<li><b>a fasta file</b> (max 500 KB) with your protein sequences.
		Only the first word after the ">" sign will be kept as protein name (<a href="'. SAMPLE_FASTA_FILE . '" data-ajax="false">sample fasta file</a>)</li>
		<li><b>a protein pairs file</b> (max 10 KB) with, for each line, two protein names, space separated (<a href="'. SAMPLE_PROTEIN_COUPLES_FILE . '" data-ajax="false">sample pairs file</a>)</li>
		</ul>
		In both files lines starting with a semicolon (;) are considered 
		comments and empty lines are ignored.<br>
		The Dscam1 server will align the Id domain variants on your proteins
		and select the best matching ones.
		</p>
		<h3>From exon file</h3>
		<p>
		Use this box if you already know which Ig domain variants are present in your protein
		or if your are not satisfied with the Dscam1 server alignments.<br>
		You are required to input one file (max 10 KB) describing, for each line, two 
		Dscam1 proteins with their Ig domains 
		coded through the corresponding exon numbers
		(<a href="'. EXON_4_FASTA . '" data-ajax="false">exon4</a>,
		<a href="'. EXON_6_FASTA . '" data-ajax="false">exon6</a>,
		and <a href="'. EXON_9_FASTA . '" data-ajax="false">exon9</a>).
		For details see Wang et al.[1].
		<br>
		The accepted input file is a csv flat file with comma separated
		columns with no quotation.
		The file needs to contain six or seven columns: three for first protein (exons 4, 6, and 9)
		three for second protein (same exon order), and an optional one
		with user notes.<br>
		Comment lines start with a semicolon (;) and blank lines are ignored.
		</p><p>
		The <a href="'. SAMPLE_EXON_FILE . '" data-ajax="false">sample exon file</a> contains six protein pairs and looks like the following:
		</p><pre>
;this is a comment line, followed by optional header<b>
;First_prot_Exon_4, First_prot_Exon_6, First_prot_Exon_9, Second_prot_Exon_4, Second_prot_Exon_6, Second_prot_Exon_9, Notes</b>
                 9,                27,                25,                  9,                 27,                 25, self binding protein_A
                 2,                27,                25,                  2,                 27,                 25, self binding protein_B
                 7,                27,                25,                  7,                 27,                 25, self binding protein_C
                 7,                48,                25,                  7,                 42,                 25, protein_D vs. others, three lines
                 7,                48,                25,                  7,                 43,                 25
                 7,                48,                25,                  7,                 44,                 25
</pre><p>
		Note that the first three lines describe self-binding cases, while others are hetero-binding.<br>
		</p>
		<h3>Results</h3>
		<p>
		Prediction results will be sent by email and will consist of a zip archive
		containing two files: a log file for errors and warnings and a csv file with binding 
		predictions that will look like the following:
		</p><pre><b>
first_prot_exons      second_prot_exons    model           predicted_binding_strength binding notes</b>
ex4.9 ex6.27 ex9.25   ex4.9 ex6.27 ex9.25  self_binding    19.24                      YES     (protein_A, protein_A)
ex4.2 ex6.27 ex9.25   ex4.2 ex6.27 ex9.25  self_binding    36.11                      YES     (protein_B, protein_B)
ex4.7 ex6.27 ex9.25   ex4.7 ex6.27 ex9.25  self_binding    26.47                      YES     (protein_C, protein_C)
ex4.7 ex6.48 ex9.25   ex4.7 ex6.42 ex9.25  hetero_binding  <4                         NO      (protein_D, protein_E)
ex4.7 ex6.48 ex9.25   ex4.7 ex6.43 ex9.25  hetero_binding  <4                         NO      (protein_D, protein_F)
ex4.7 ex6.48 ex9.25   ex4.7 ex6.44 ex9.25  hetero_binding  &ge;4                         YES     (protein_D, protein_G)
ex4.7 ex6.48 ex9.25   NA NA NA             NA              NA                         NA      (protein_D, RND5404) See log for details.
ex4.9 ex6.35 ex9.3    ex4.9 ex6.35 ex9.3   self_binding    16.83                      YES     (tr|Q0E9L5|Q0E9L5_DROME, tr|Q0E9L5|Q0E9L5_DROME)
ex4.9 ex6.35 ex9.3    ex4.9 ex6.27 ex9.25  hetero_binding  <4                         NO      (tr|Q0E9L5|Q0E9L5_DROME, protein_A)
		</pre>
	With fields:
	<ul>
	<li><b>first_prot_exons, second_prot_exons</b>: the identified Ig domains variants, either directly
	read from the exon file or coming from the alignment of protein sequences</li>
	<li><b>model</b>: either "hetero_binding" or "self_binding"</li>
	<li><b>predicted_binding_strength</b>: the predicted binding strenght (the higher the
	stronger)</li>
	<li><b>binding</b>: boolean telling if the two proteins are considered
	binding or not</li>
	<li><b>notes</b>: the protein names are reported here if present. If any problem is
	present with alignment or with the model itself a note invites the user to 
	consult the log file.</li>
	</ul>
	The value "NA" (Not Available) is used to fill empty cells, and indicates that
	some kind error occurred in the identification of Ig domains variants.';
	echo build_data_block('How to use', $text, 'collapsible', 'ui-content', 'data-collapsed="true"');
}

function put_single_run(){
	$ex4 = get_fake_exon_variants(12);
	$ex6 = get_fake_exon_variants(48);
	$ex9 = get_fake_exon_variants(33);
	
	#form starts
	$text = '<form method="post" action="res.php">';
	
	#we are running in single mode
	$text .= '<input type="hidden" name="mode" value="single_run">';
	
	#protein one starts
	$text .= '<div class="protein-label">First protein</div>
			  <fieldset class="ui-field-contain">';
	#protein one exons
	$text .= '
	<label for="p1_ex4">Exon 4</label>
	<select name="p1_ex4" id="p1_ex4">'.build_option_list($ex4).'</select>
	<label for="p1_ex6">Exon 6</label>
	<select name="p1_ex6" id="ex6">'.build_option_list($ex6).'</select>
	<label for="p1_ex9">Exon 9</label>
	<select name="p1_ex9" id="p1_ex9">'.build_option_list($ex9).'</select>
	';
	
	#protein one ends, protein two starts
	$text .= '</fieldset>';
	$text .= '<div class="protein-label">Second protein</div>
			  <fieldset class="ui-field-contain">';

	#protein two exons
	$text .= '
	<label for="p2_ex4">Exon 4</label>
	<select name="p2_ex4" id="p2_ex4">'.build_option_list($ex4).'</select>
	<label for="p2_ex6">Exon 6</label>
	<select name="p2_ex6" id="ex6">'.build_option_list($ex6).'</select>
	<label for="p2_ex9">Exon 9</label>
	<select name="p2_ex9" id="p2_ex9">'.build_option_list($ex9).'</select>
	';
	
	#form ends
	$text .= '</fieldset><input type="submit" data-inline="true" value="Submit"></form>';        
	
	echo build_data_block('single run', $text, 'collapsible', 'ui-content', 'data-collapsed="true"');
}

function put_bulk_run_from_sequence(){
	//form starts
	$text = '<form method="post" action="res.php" enctype="multipart/form-data" data-ajax="false">';

	//we are running in bulk mode from sequence
	$text .= '<input type="hidden" name="mode" value="bulk_run_from_sequence">';

	//enter the fasta file with proteins sequences
	$text .= '<label for="inputproteins_fasta">';
	$text .= 'Input fasta file (max 500 KB) with proteins amino acid sequences';
	$text .= ' (see <a href="'. SAMPLE_FASTA_FILE . '" data-ajax="false">sample fasta file</a>)';
	$text .= ':</label>';
	$text .= '<input type="file" name="inputproteins_fasta" id="inputproteins_fasta" />';
	
	//enter the text file with proteins pairs
	$text .= '<label for="inputproteins_pairs">';
	$text .= 'Input file (max 10 KB) with proteins pairs';
	$text .= ' (see <a href="'. SAMPLE_PROTEIN_COUPLES_FILE . '" data-ajax="false">sample pairs file</a>)';
	$text .= ':</label>';
	$text .= '<input type="file" name="inputproteins_pairs" id="inputproteins_pairs" />';
	
	//enter the email
	$text .= '<label for="email">Email (for results):</label>';
	$text .= '<input type="text" name="email" id="email">';
	
	//should we put the reCAPTCHA?
	if (DO_RECAPTCHA){
		//https://developers.google.com/recaptcha/intro
		$text .= '<div id="Recaptcha_bulk_seq" class="g-recaptcha"></div>';
	}
	
  	//submit button
	$text .= '<input type="submit" data-inline="true" value="Submit">';
		
	//form ends
	$text .= '</form>';
	
	echo build_data_block('Run the model from sequence file', $text, 'collapsible', 'ui-content', 'data-collapsed="false"');
}

function put_bulk_run(){
	//form starts
	$text = '<form method="post" action="res.php" enctype="multipart/form-data" data-ajax="false">';

	//we are running in bulk mode
	$text .= '<input type="hidden" name="mode" value="bulk_run">';

	//enter the csv file
	$text .= '<label for="inputproteins">';
	$text .= 'Input file (max 10 KB) with proteins exons variants';
	$text .= ' (see <a href="'. SAMPLE_EXON_FILE . '" data-ajax="false">sample exon file</a>)';
	$text .= ':</label>';
	$text .= '<input type="file" name="inputproteins" id="inputproteins" />';
	
	//enter the email
	$text .= '<label for="email">Email (for results):</label>';
	$text .= '<input type="text" name="email" id="email">';
	
	//should we put the reCAPTCHA?
	if (DO_RECAPTCHA){
		//https://developers.google.com/recaptcha/intro
		$text .= '<div id="Recaptcha_bulk" class="g-recaptcha"></div>';
	}
	
  	//submit button
	$text .= '<input type="submit" data-inline="true" value="Submit">';        
		
	//form ends
	$text .= '</form>';
	
	echo build_data_block('Run the model from exon file', $text, 'collapsible', 'ui-content', 'data-collapsed="false"');
}

function build_data_block($title, $body, $role='collapsible', $class='ui-content', $extraprop = ''){
        $res = '<div data-role="'.$role.'" class="'.$class.'" '.$extraprop.'>';
        if ($title != '') $res .= '<h1>'.$title.'</h1>';
        if ($body != '') $res .= $body;
        $res .= '</div>';
        
        return $res;
}

function build_option_list($named_array){
        $res = '';
        foreach ($named_array as $key => $value){
                $res .= '<option value="'.$key.'">'.$value.'</option>';
        }
        return $res;
}

function get_fake_exon_variants($num){
        $res = array();
        for ($i =0; $i<$num; $i++){
                $res['variant'.($i+1)] = 'variant'.($i+1);
        }
        
        return $res;

}

/*
 * This function checks the g-recaptcha-response from the form, contacting
 * google's server, and returns an array of error messages (if something
 * went wrong) or an empty array (if all was good)
 */
function check_reCaptcha($response){
	$errors = array();
	
	$secretKey = RECAPTCHA_SECRETKEY;
	$ip = $_SERVER['REMOTE_ADDR'];
	$result=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$response."&remoteip=".$ip);
	$responseKeys = json_decode($result,true);
	if(intval($responseKeys["success"]) !== 1) {
		$errors[] = "Re-captcha failed. Are you a robot?";
		
		/* Once upon a time we printed several details about the error,
		 * but now we think it's useless without a proper log...
		 * 
		 * OLD ERRORS
		 * - challenge timestamp : $responseKeys[challenge_ts]
		 * - hostname            : $responseKeys[hostname]
		 * - error codes         : implode(' ', $responseKeys['error-codes']
		 * */

	} 
	
	return ($errors);
}

/*parse content of user input from $_POST and $_FILES.
 * Returns an array with two keys:
 * - 'errors' => array of error messages to be displayed, if something
 *               went wrong during the parsing
 * - 'values' => the parsed values, can have substructure */
function parse_input(){
	$errors = array();
	$values = array();
	
	//should we do the reCaptcha test?
	if (DO_RECAPTCHA){
		$errors = array_merge($errors, check_reCaptcha($_POST['g-recaptcha-response']));
	}
	
	//checking the allowed modes
	$mode_found = FALSE;
	
	//bulk run
	if ($_POST['mode'] === 'bulk_run'){
		$mode_found = TRUE;
		$values['mode'] = $_POST['mode'];
		
		//we need to ensure the passed file is ok. The check results
		//go straight to the error list
		$errors = array_merge(
			$errors, 
			check_file($_FILES['inputproteins'], MAX_FILE_SIZE_EXONS, 'Exon file: ')
		);
		
		//in any case the reference filename is written
		$infile = $_FILES['inputproteins']['tmp_name'];
		
		//if we get to this point without errors, we should parse the input file
		if (count($errors) == 0){
			$parsing_res = parse_protein_description($infile, 'Exon file: ');
			$errors = $parsing_res['errors'];
			$values['proteins'] = $parsing_res['proteins'];
			$values['self_binding'] = $parsing_res['self_binding'];
			$values['notes'] = $parsing_res['notes'];
		}
		
		//we need to check input email, too
		$errors = array_merge($errors, check_email($_POST['email']));
		$values['mail'] = $_POST['email'];		
	}

	//bulk run with input as sequence
	if ($_POST['mode'] === 'bulk_run_from_sequence'){
		$mode_found = TRUE; 
		$values['mode'] = $_POST['mode'];
		
		//we need to ensure the passed file is ok, and the check results
		//go straight to the error list
		$errors = array_merge(
			$errors, 
			check_file($_FILES['inputproteins_fasta'], MAX_FILE_SIZE_SEQUENCE, 'Protein sequence file: ')
		);
		$errors = array_merge(
			$errors, 
			check_file($_FILES['inputproteins_pairs'], MAX_FILE_SIZE_PAIRS, 'Protein pairs file: ')
		);

		//if we get to this point without errors, we should parse the input file
		if (count($errors) == 0){
			//parsing
			$parsing_res_sequence = parse_fasta($_FILES['inputproteins_fasta']['tmp_name'], 'Protein sequence file: ');
			$parsing_res_pairs    = parse_pair_file(
				$_FILES['inputproteins_pairs']['tmp_name'], 
				array_keys($parsing_res_sequence['proteins']),
				'Protein pairs file: '
			);

			//keeping track of errors
			$errors = array_merge($errors, $parsing_res_sequence['errors'], $parsing_res_pairs['errors']);
			
			//keeping track of parsed data
			$values['sequences'] = $parsing_res_sequence['proteins'];
			$values['pairs'] = $parsing_res_pairs['pairs'];
		}
		
		//we need to check input email, too
		$errors = array_merge($errors, check_email($_POST['email']));
		$values['mail'] = $_POST['email'];		
	}
	
	//are we in a valid mode?
	if (!$mode_found){
		$errors[] = 'unknown mode selected: '.$_POST['mode'];
	}

	//and we are done
	return array('errors' => $errors, 'values' => $values);
}

?>
